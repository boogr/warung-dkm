import Link from 'next/link';

const App = () => {
    return (
        <div className="header">
            <div className="navigation">
                <Link href="/">
                    <a>Home</a>
                </Link>
                <Link href="/page1">
                    <a>Page 1</a>
                </Link>
                <Link href="/page2">
                    <a>Page 2</a>
                </Link>
                <Link href="/about">
                    <a>About</a>
                </Link>
            </div>
        </div>
    );
}

export default App;