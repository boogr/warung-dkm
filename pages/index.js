import { useState } from 'react';
import Navigation from '../components/navigation';

const list = [
  "Item 01",
  "Item 02",
  "Item 03",
  "Item 04",
  "Item 05",
  "Item 06",
];

const App = () => {
  const [value, setValue] = useState('-');

  const onChangeSelected = (x) => {
    setValue(x);
  }

  return (
    <div>
      <Navigation />
      <h1>Sample Home</h1>
      {list.map((x, idx) => {
        if (x === value) {
          return (
            <div style={{
              fontSize: 18,
              padding: '5px 20px',
              fontWeight: 'bold',
              color: 'blueviolet',
            }}>{x}</div>
          )
        }

        return (
          <div onClick={() => { onChangeSelected(x) }} style={{
            fontSize: 18,
            padding: '5px 20px',
            cursor: 'pointer'
          }}>{x}</div>
        )
      })}
      <br />
      <br />
      <br />
      Anda memilih {value}
    </div>
  );
}

export default App;