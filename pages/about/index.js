import Navigation from '../../components/navigation';

const App = () => {
    return (
        <div>
            <Navigation />
            <h1>About</h1>
        </div>
    );
}

export default App;